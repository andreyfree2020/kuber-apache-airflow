The official Helm chart to deploy Apache Airflow, a platform to programmatically author, schedule, and monitor workflows
https://artifacthub.io/packages/helm/apache-airflow/airflow

From official web-site:
- Helm Chart for Apache Airflow
https://airflow.apache.org/docs/helm-chart/stable/index.html

- Parameters reference
https://airflow.apache.org/docs/helm-chart/stable/parameters-ref.html

- Workflows as code
https://airflow.apache.org/docs/apache-airflow/stable/index.html